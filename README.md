# Von-QBE
##### *Virtual Ontology Query by Example*
---

This repository is only for publishing a builded docker image. To see and contrib to the code, access [GitHub](https://github.com/InsightLab/von-qbe).

To run a container with Von-QBE, just use the following command:
```docker run -e PORT=<port> -e IP=<public_ip> -p <port>:8000 -t registry.gitlab.com/lucas.peres/vonqbe``` ,
Where:
* **PORT** is the port that you will consume the application;
* **IP** is the service public IP. This IP is very important, since the web page will use-it to consume the backend service.

**WARNING**: if you use something to **redirect** the requests to this service, you must know that both **HTTP** and **WS** protocols are used, so both must be redirected.